package ru.mazur.controller;

import ru.mazur.service.UserService;
import ru.mazur.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserController extends HttpServlet {

    UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        //String password = request.getParameter("password");

        userName = userService.somethingBisnessLogic(userName);

        PrintWriter out = response.getWriter();
        out.print("<html><body>");
        out.print("<h2>Welcome " + userName + "</h2>");
        out.print("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
