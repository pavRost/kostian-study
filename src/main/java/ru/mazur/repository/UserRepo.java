package ru.mazur.repository;

public interface UserRepo {

    public String getDataFromDataBase(String userName);
}
