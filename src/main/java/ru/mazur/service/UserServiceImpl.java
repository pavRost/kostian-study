package ru.mazur.service;

import ru.mazur.repository.UserRepo;
import ru.mazur.repository.UserRepoImpl;

public class UserServiceImpl implements UserService {

    private UserRepo userRepo = new UserRepoImpl();

    public String somethingBisnessLogic(String userName) {
        return userRepo.getDataFromDataBase(userName);
    }
}
